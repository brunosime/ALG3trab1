#include<stdio.h>
#include<stdlib.h>

//AVL struct instance;
typedef struct node{

    int key;
    int depth;
    struct node *L_son;
    struct node *R_son;
    struct node *father;
    
}node;


//AVL's functions and procedures;
node *createAVL(int key);
node *insert(node *n, int key);
node *balancer(node *n);
node *delete(node *n, node *target);
node *L_rotate(node *n);
node *R_rotate(node *n);
node *RL_rotate(node *n);
node *LR_rotate(node *n);
int bigger(int x, int y);
int height(node *n);
int balanceFactor(node *n);
node *binarySearch(node *n, int key);
node *successor(node *n);
void in_order(node *n);
void printAVL(node *n, int depth);
void menu();