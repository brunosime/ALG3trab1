#include "source/myAVL.h"



void main(){

    system("clear");

    char op;
    int key;
    node *avl=NULL;

    printf("Bilioteca de opercoes Adelson-Velsky & Landis em BST\n");
    printf("Codigo desenvolvido como requisito de conclusão à disciplina CI057 - UFPR.\n");
    printf("Alunos Bruno Sime (GRR20190568) e Isabela Barata (GRR20213395)\n");
    printf("Sob supervisão do docente Eduardo Cunha de Almeida, DInf - UFPR.\n");
    printf("\n\n\n");

    menu();
    scanf("%c", &op);
    while(op=='i'||op=='r'||op=='p'||op=='t' || op!='s'){
        switch(op){
            case 'i':
                printf("informe o valor a ser inserido na AVL: ");
                scanf("%d", &key);
                avl=insert(avl, key);
                system("clear");
                menu();
            break;
            case 'r':
                printf("insira a chave do elemento a ser removido: ");
                scanf("%d", &key);
                delete(avl, binarySearch(avl, key));
                system("clear");
                menu();
            break;
            case 'p':
                system("clear");
                printAVL(avl, 1);
                printf("\n");
                menu();
            break;
            case 't':
                system("clear");
                in_order(avl);
                printf("\n");
                menu();
            break;
        }
        scanf("%c", &op);
    }
    free(avl);
    return;
}